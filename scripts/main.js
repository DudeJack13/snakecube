//Setup engine and scene
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
var keyboard = new THREEx.KeyboardState();

var renderer = new THREE.WebGLRenderer();
renderer.setClearColor(0xffffff, 1);
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;
renderer.gammaOutput = true

var controls = new THREE.OrbitControls(camera);
var loader = new THREE.GLTFLoader();
var clock = new THREE.Clock();
document.body.appendChild(renderer.domElement);

<<<<<<< HEAD
=======
var cubes = [];
var movePads = [];
var cIDs = [
	[-1, -1, -1, -1],
	[-1, -1, -1, -1],
	[-1, -1, -1, -1],
	[-1, -1, -1, -1],
	[2, 1, 0, 3],
	[-1, -1, -1, -1]
];
for (var a = 0; a < 6; a++) {
	var array = [];
	var i = {
		points: array,
		connectionIDs: cIDs[a],
	}
	movePads.push(i);
}
var centerCube;
var loaded = 0;
var amountToLoad = 0;
var loadLock = false;
var loading = true;
var debug = true;

var player = {
	model: null,
	gridX: null,
	gridY: null,
	section: null,
	direction: 1,
	speed: 2,
	speedCounter: 0,
	speedCounterMax: 1
}

var loadingText = document.createElement('div');
loadingText.style.position = 'absolute';
loadingText.innerHTML = "Loading...";
loadingText.style.top = 10 + 'px';
loadingText.style.left = 10 + 'px';
document.body.appendChild(loadingText);

var LoadCube = function (x, y, z, section) {
	amountToLoad++;
	loader.load('cube.glb',
		function (gltf) {
			var c = {
				gridX: x,
				gridY: y,
				gridZ: z,
				section: section,
				model: gltf.scene,
			};
			cubes.push(c);
			scene.add(cubes[cubes.length - 1].model);
			centerCube.add(cubes[cubes.length - 1].model);
			cubes[cubes.length - 1].model.position.x = (x - 2) * 2;
			cubes[cubes.length - 1].model.position.y = (y - 2) * 2;
			cubes[cubes.length - 1].model.position.z = (z - 2) * 2;
			loaded++;
		}
	);
}

var LoadMovePoints = function (x, y, z, gx, gy, section) {
	amountToLoad++;
	var c = {
		gridX: gx,
		gridY: gy,
		worldX: (x - 2) * 2,
		worldY: (y - 2) * 2,
		worldZ: (z - 2) * 2,
		section: section,
	};
	movePads[section].points.push(c);
	if (debug) {
		loader.load('centercube.glb',
			function (gltf) {
				gltf.scene.position.set((x - 2) * 2, (y - 2) * 2, (z - 2) * 2);
				centerCube.add(gltf.scene);
				scene.add(gltf.scene);
				loaded++;
			}
		);
	}
}

var LoadGameCube = function () {
	//Build starting cube
	var h = 0;
	for (var l = 0; l < 5; l++) {
		for (var w = 0; w < 5; w++) {
			LoadCube(l, h, w, 0);
		}
	}
	h++;
	for (var h1 = h; h1 < 4; h1++) {
		var l1 = 0;
		for (var w = 0; w < 5; w++) {
			LoadCube(l1, h1, w);
		}
		l1 = 4;
		for (var w = 0; w < 5; w++) {
			LoadCube(l1, h1, w);
		}
		var w1 = 0;
		for (var l = 1; l <= 3; l++) {
			LoadCube(l, h1, w1);
		}
		w1 = 4;
		for (var l = 1; l <= 3; l++) {
			LoadCube(l, h1, w1);
		}
	}
	h += 3;
	for (var l = 0; l < 5; l++) {
		for (var w = 0; w < 5; w++) {
			LoadCube(l, h, w);
		}
	}

	//Build movable pads
	for (var a = 0; a < 7; a++) {
		for (var b = 0; b < 7; b++) {
			LoadMovePoints(a - 1, 5, b - 1, a, b, 4);
			//LoadMovePoints(a, -1, b, a, b, 5);
			LoadMovePoints(5, a - 1, b - 1, a, b, 1);
			//LoadMovePoints(-1, a, b, a, b, 3);
			//LoadMovePoints(a, b, 5, a, b, 0);
			//LoadMovePoints(a, b, -1, a, b, 2 );
		}
	}
}

var GetMovePoint = function (spawnPad, x, y) {
	if (typeof movePads[spawnPad] !== 'undefined') {
		for (var i = 0; i < movePads[spawnPad].points.length; i++) {
			if (movePads[spawnPad].points[i].gridX == x && movePads[spawnPad].points[i].gridY == y) {
				return [i];
			}
		}
	} else {
		console.log("GetMovePoint: Spawnpad == NULL");
	}
	return null;
}

var LoadPlayer = function () {
	amountToLoad++;
	loader.load('snakeheadcube.glb',
		function (gltf) {
			var showPad = 4;
			var i = GetMovePoint(showPad, 1, 1);
			player.model = gltf.scene;
			player.model.position.set(movePads[showPad].points[i].worldX, movePads[showPad].points[i].worldY, movePads[showPad].points[i].worldZ);
			player.gridX = movePads[showPad].points[i].gridX;
			player.gridY = movePads[showPad].points[i].gridY;
			player.section = showPad;
			player.direction = 1;
			centerCube.add(player.model);
			scene.add(player.model);
			loaded++;
		}
	);
}

var ChangeCubeSide = function() {
	for (var p = 0; p < movePads.length; p++) { //Each pad
		if (p != player.section) {
			for (var po = 0; po < movePads[p].points.length; po++) {
				if (
					movePads[p].points[po].worldX == player.model.position.x &&
					movePads[p].points[po].worldY == player.model.position.y &&
					movePads[p].points[po].worldZ == player.model.position.z
				) {
					console.log("hit at P:" + p + " PO:" + po);
					player.gridX = movePads[p].points[po].gridX;
					player.gridZ = movePads[p].points[po].gridZ;
					player.section = p;
					return;
				}

			}
		}
	}
}

>>>>>>> 8cd452fc0a53b870a064bdeaa74c0bc2fbc38abd
//Load
var Load = function () {

}

//Update every frame
var Update = function () {
	frameTime = clock.getDelta();
	requestAnimationFrame(Update);
	renderer.render(scene, camera);
<<<<<<< HEAD
=======

	if (loading) {
		loadingText.innerHTML = "Loading: " + loaded + " / " + amountToLoad + " loaded";
		if (loaded == amountToLoad && loadStage == 1) {
			console.log("Loading cube");
			LoadGameCube();
			loadStage = 2;
		} else if (loaded == amountToLoad && loadStage == 2) {
			console.log("Loading Player");
			LoadPlayer();
			loadStage = 3;
		} else if (loaded == amountToLoad && loadStage == 3) {
			console.log(movePads);
			console.log(player);
			loading = false;
		}
	} else {
		loadingText.innerHTML = "Loading: " + loaded + " / " + amountToLoad + " loaded<br/>Loading complete";
		controls.update();

		player.speedCounter += player.speed * frameTime;

		if (player.speedCounter > player.speedCounterMax) {
			player.speedCounter = 0;
			if (keyboard.pressed("a")) {
				player.gridX--;
			} else if (keyboard.pressed("d")) {
				player.gridX++;
			}
			if (player.section != -1) {
				var i = GetMovePoint(player.section, player.gridX, player.gridY);
				if (i != null) {
					player.model.position.set(
						movePads[player.section].points[i[0]].worldX,
						movePads[player.section].points[i[0]].worldY,
						movePads[player.section].points[i[0]].worldZ
					);
				} else {
					console.log("Hit side of " + player.section);
					//TO-DO search only one side
					ChangeCubeSide();
				}
			}
		}
	}
>>>>>>> 8cd452fc0a53b870a064bdeaa74c0bc2fbc38abd
};

Load();
Update();